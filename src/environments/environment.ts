// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDGu3ky7GPMBFubd_5Rwqf4ltmH2f_qZgE",
    authDomain: "travel-plans-app.firebaseapp.com",
    databaseURL: "https://travel-plans-app.firebaseio.com",
    projectId: "travel-plans-app",
    storageBucket: "travel-plans-app.appspot.com",
    messagingSenderId: "279309291705",
    appId: "1:279309291705:web:96a9cc1c1aae4883124b68"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
