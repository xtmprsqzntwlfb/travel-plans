import { Injectable } from '@angular/core';
import { Trip } from '../model/trip';
import { database } from 'firebase/app';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

const DB_TRIPS_PATH = 'trips';

@Injectable({
  providedIn: 'root'
})
export class TripsService {

  private tripsDb: AngularFireList<Trip>;

  constructor(private db: AngularFireDatabase) {
    this.tripsDb = this.db.list(`/${DB_TRIPS_PATH}`);
  }

  getTripsList(): Observable<Trip[]> {
    return this.tripsDb.snapshotChanges()
      .pipe(map((changes) => {
        return changes.map(change => ({
          key: change.payload.key,
          ...change.payload.val()
        }));
      }));
  }

  getTrip(key: string): Observable<Trip> {
    return new Observable<Trip>(subscribe => {
      this.getTripsList()
        .pipe(first())
        .subscribe(trips => {
          for (const trip of trips) {
            if (trip.key === key) {
              subscribe.next(trip);
              subscribe.complete();
              return;
            }
          }
        });
      });
    }

  addTrip(trip: Trip): database.ThenableReference {
    return this.tripsDb.push(trip);
  }

  removeTrip(key: string): Promise<void> {
    return this.tripsDb.remove(key);
  }

  updateTrip(key: string, values: Partial<Trip>): Promise<void> {
    const tripObject = this.db.object(`/${DB_TRIPS_PATH }/${key}`);
    return tripObject.update(values);
  }
}
