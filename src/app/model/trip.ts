export interface Trip {
  key?: string;
  destination: string;
}
