import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Trip } from './../../model/trip';
import { first } from 'rxjs/operators';
import * as _ from 'lodash';
import { TripsService } from './../../services/trips.service';

@Component({
  selector: 'app-trip-edit',
  templateUrl: './trip-edit.component.html',
  styleUrls: ['./trip-edit.component.css']
})
export class TripEditComponent implements OnInit {
  tripForm = this.createForm();

  isBusy = false;
 
  get isNew(): boolean {
    return _.isNil(this.key);
  }
 
  private key?: string;
 
  constructor(private router: Router, private route: ActivatedRoute, private trips: TripsService) { }
 
  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('key');
 
    if (!this.isNew) {
      this.trips.getTrip(this.key)
        .pipe(first())
        .subscribe(trip => {
          this.tripForm = this.createForm(trip);
        });
    }
  }
 
  onSubmit() {
    this.isBusy = true;
     if (this.isNew) {
      this.trips.addTrip(this.tripForm.value)
        .then(() => {
          this.isBusy = false;
          this.router.navigate(['']);
        });
    }
    else {
      this.trips.updateTrip(this.key, this.tripForm.value)
        .then(() => {
          this.isBusy = false;
          this.router.navigate(['']);
        });
    }
  }

  onCancelClick() {
    this.router.navigate(['']);
  }

  private createForm(trip?: Trip): FormGroup {
    return new FormGroup({
      destination: new FormControl(trip?.destination, [Validators.required])
    });
  }
}