import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject, pipe } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { TripsService } from './../../services/trips.service';
import { Trip } from './../../model/trip'
import { ConfirmationDialogComponent } from './../../views/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.css']
})
export class TripListComponent implements OnInit, OnDestroy {

  data: Trip[];

  displayedColumns = ['destination', 'edit'];

  private destroy$ = new Subject();

  constructor(private trips: TripsService, private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.trips.getTripsList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((trips: Trip[]) => {
        this.data = trips;
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  onAddTrip() {
    this.router.navigate(['/trip']);
  }

  onEdit(key: string) {
    this.router.navigate(['/trip', key]);
  }

  onRemove(destination:string, key: string) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: {
          title: 'Delete for all eternity?',
          message: `Delete the trip "${destination}"?`
        }
    });
      
    dialogRef.afterClosed()
      .pipe(first())
      .subscribe(result => {
        result && this.trips.removeTrip(key);
      });
  }

}
