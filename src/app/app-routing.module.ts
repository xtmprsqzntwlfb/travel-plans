import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { TripEditComponent } from './views/trip-edit/trip-edit.component';


const routes: Routes = [{
  component: TripEditComponent,
  path: "trip/:key"
}, {
  component: TripEditComponent,
  path: "trip"
}, {
  component: HomeComponent,
  path: "",
  pathMatch: "full"
}, /*{
  component: HomeComponent,
  path: "**"
}*/];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
